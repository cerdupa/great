!**********************************************************************
! Test program (test_file):
! Example of the use of the module analysis module reading data from a
!  file. The file contains data from a polytropic TOV star (Gamma=2
!  , K=100) with ~1.4 M_sun.
!**********************************************************************
!
! GPL  License:
! -------------
!
!    GREAT = General Relativistic Eigenmode Analysis Tool
!    Copyright (C) 2023 Alejandro Torres-Forne and Pablo Cerda-Duran
!
!    This file is part of GREAT.
!
!    GREAT is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    GREAT is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with GREAT.  If not, see <http://www.gnu.org/licenses/>.
!
!************************************************************************

program main

  use module_param
  use module_background
  use module_eigen
  use module_mode_analysis


  implicit none

  integer :: i, nt

  !> parameters 
  type (parameters_t) :: param  
  !> backgroun data (1D profiles)
  type(BGData_t) :: data

  !> error flag
  integer :: ierr

  !> Size of the grid (without ghost cells)
  integer :: m
  !> index location of the shock (last cell inside the shock)
  integer :: iR


  !> variables to read data from file
  integer :: mmax
  real*8, allocatable, dimension (:) :: r, rho, p, eps, cs2, alpha, phi
  
  real*8 :: tmp1, tmp2

  character (len=255) :: parfile

  mmax=10000
  allocate(r (mmax))
  allocate(rho (mmax))
  allocate(p (mmax))
  allocate(eps (mmax))
  allocate(cs2 (mmax))
  allocate(alpha (mmax))
  allocate(phi (mmax))
  

  

  ! ---- Read parameter file  -------
  parfile="./parameters"
  call Read_parameters(param, parfile, ierr)
  if (ierr.ne.0) stop

  

  ! ------ nt loops over different times of the simulation. In this example ---
  ! ------ we compute only one time -------------------------------------------
  do nt = 1, 1
          
     open (unit=10, file=trim(param%input_directory)//"/tov1.4.dat")
     read (10, *)
     m=0
     do i = 1, mmax
        read (10, *, end=100) tmp1, r(i), rho(i), p(i), eps(i), cs2(i), alpha(i), tmp2, phi(i)
        m = m+1
     enddo
100  continue
     close(10)
     print*, "m    = ", m
     print*, "Riso =", r (m)/1e5, " km"

     ! ...... Allocate arrays ................
     call Init_background_data (data, m, ierr)
     

     ! ..... not used. Just to keep track of where the data comes from 
     data%input_directory = param%input_directory
     ! ..... Output directory. 
     data%output_directory = param%output_directory
    
     ! time index
     data%nt = nt
     ! simulation time (since this example only has one time we set it to zero)
     data%time = 0.0d0
     ! index location of the shock (last cell inside the shock)
     ! for this example we choose it to be equal to the last cell
     ! in the grid, but does not have to be like this.
     data%iR = m
     
     
     ! ..... Fill arrays with background data ............
     ! We use units of G=c=1 and the remaining length scale in cm.
     do i = 1, m
        ! ... radius (in cm)
        data%r (i) =  r (i)
        ! ... rest mass density (cm^-2)
        data%rho (i) = rho (i)
        ! ... sound speed squared (dimensionless)
        data%c_sound_squared (i) = cs2 (i)
        ! ... pressure (cm^-2)
        data%p (i) = p (i)
        ! ... specific internal energy (dimensionless)
        data%eps (i) = eps (i)
        
        ! ---- Metric: set to mikowsky -----
        ! conformal factor (dimensionless)
        data%phi (i) =  phi (i) 
        ! lapse (dimensionless)
        data%alpha (i) = alpha (i)
     enddo

     ! If this flag is set to true, it indicates that the data does not contain the value
     ! of N^2 (Brunt-Vaisala frequency) and will be computed internally.
     data%calculate_n2 = .true.

     ! ...... Fill ghost cells ...........
     call Impose_boundary_conditions_bg (data, ierr)

     ! ....... Compute eigenmodes ........
     ! It computes eigenfrequencies and eigenmodes.
     ! Eigenfrequencies are appended to the freqs.dat file.
     ! Eigenfunctions for each time/nt are stored into  
     ! eigen_<nt>.dat. All output in the output_directory.
     call Perform_mode_analysis (data, param, ierr)

     ! ......... Prints an error message if ierr != 0 .....
     call Print_error_message (ierr)


     ! ........ Output background quantities .........
     ! It includes not only the values set above but also
     ! internally calculated quantities such as the Brunt-Vaisala
     ! frequency and the Lamb frequency
     call Output_background_data(data, param, ierr)

     ! ......... Deallocate arrays in data ..........
     call Free_background_data (data, ierr)

  enddo

  deallocate(r)
  deallocate(rho)
  deallocate(p)
  deallocate(eps)
  deallocate(cs2)
  deallocate(alpha)
  deallocate(phi)
  
end program main



