# Based in the code created by Sebastiano Bernuzzi: 
# https://colab.research.google.com/drive/1yMD2j3Y6TcsykCI59YWiW9WAMW-SPf12

import numpy as np



def EOS_p (p, K=100., Gamma=2.):
  """
  Equation of state (EOS)
  Given pressure, return energy density, rest-mass density and sound speed
  """      
  ene = (p/K)**(1./Gamma) + p/(Gamma-1.)
  rho = (p/K)**(1./Gamma)                       
  cs2 = K*Gamma*(Gamma-1)/(Gamma-1 + K*Gamma*rho**(Gamma-1))*rho**(Gamma-1)
  return ene, rho, cs2

def EOS_rho(rho, K=100., Gamma=2.):
  """
  Equation of state (EOS)
  Given rest-mass density, return energy density and pressure
  Polytropic EOS: P = k rho^Gamma
  """
  p = K*rho**Gamma
  e = rho + p/(Gamma-1.);
  return p, e


def tov (rho0, K=100., Gamma=2., N=100, rmin=1e-6,rmax=20):
  # rho0: Central density
  # K: Polytropic constant
  # Gamma: Polytropic (adiabatic) index
  # rmin: location of innermost grid point
  # rmax: location of outermost grid point
  # N: number of points between rmin and rmax

  
  r = np.linspace(rmin,rmax,N)
  
  dr = np.diff(r)[0] # assume equally spaced
  m=np.zeros(N) # Mass within radius
  p=np.zeros(N) # pressure
  rho=np.zeros(N) # rest mass density
  e=np.zeros(N) # energy density density
  cs2=np.zeros(N) # sound speed square

  m[0] = 4./3.*np.pi*e0*rmin**3
  p[0], e[0] = EOS_rho(rho0, K, Gamma)
  
  SurfaceFound=False
  for i in range(N-1):
    if not SurfaceFound: # Interior
      # Call the EOS to get e
      e[i], rho[i], cs2[i]  = EOS_p (p[i], K, Gamma)
      # Solve TOV equations
      m[i+1] = m[i] + dr * 4*np.pi*e[i]*r[i]**2                               
      p[i+1] = p[i] - dr *(e[i]+p[i])*(m[i] + 4*np.pi*r[i]**3*p[i])/(r[i]*(r[i]-2*m[i]))
      isurf=i
      if p[i+1] <= 0. : # Surface found
        SurfaceFound=True
    else:   # Exterior
      m[i]=m[i-1]
      p[i]=0.
      rho[i]=0.
      e[i]=0.
      cs2[i]=0.
      

  R = r[isurf] # Msun
  M = m[isurf] # Msun

  alpha = np.sqrt(1-2*m/(r))
  grr = 1./alpha**2


  # Schwarzschild exterior in isotropic coordinates expressed as a function of the Schwarzschild radius
  phi = r/M * (1. - np.sqrt(1.-2*M/rs))
  log_phi = np.log(phi)

  # Integrate in the interior (from the surface to the center)
  for i in np.arange(N-2, -1, -1):
    if p[i]>0:
      log_phi[i] = log_phi[i+1] - dr * 0.5 * (1-np.sqrt(grr[i+1]))/r[i]
      phi[i] = np.exp(log_phi[i])

  riso=rspan/phi**2
  Riso=riso[imax]
  
  return R, Riso, M, r, riso, rho, p, ene, cs2, alpha, grr, m, phi
