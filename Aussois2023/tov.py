# Based in the code created by Sebastiano Bernuzzi: 
# https://colab.research.google.com/drive/1yMD2j3Y6TcsykCI59YWiW9WAMW-SPf12

import numpy as np
from scipy.interpolate import CubicSpline


def EOS_p (p, K=100., Gamma=2.):
  """
  Equation of state (EOS)
  Given pressure, return energy density, rest-mass density and sound speed
  """      
  ene = (p/K)**(1./Gamma) + p/(Gamma-1.)
  rho = (p/K)**(1./Gamma)                       
  cs2 = K*Gamma*(Gamma-1)/(Gamma-1 + K*Gamma*rho**(Gamma-1))*rho**(Gamma-1)
  return ene, rho, cs2

def EOS_rho(rho, K=100., Gamma=2.):
  """
  Equation of state (EOS)
  Given rest-mass density, return energy density and pressure
  Polytropic EOS: P = k rho^Gamma
  """
  p = K*rho**Gamma
  e = rho + p/(Gamma-1.);
  return p, e


def tov (rho0, K=100., Gamma=2., Nout=100, rmax=20):
  # rho0: Central density
  # K: Polytropic constant
  # Gamma: Polytropic (adiabatic) index
  # rmin: location of innermost grid point
  # rmax: location of outermost grid point
  # N: number of points between rmin and rmax for the output

  stp=100
  N=Nout*stp

  # Staggered grid (no points at 0 or rmax)
  dr=rmax/N
  r = np.arange(N)*dr + dr*0.5
  
  m=np.zeros(N) # Mass within radius
  p=np.zeros(N) # pressure
  rho=np.zeros(N) # rest mass density
  e=np.zeros(N) # energy density density
  cs2=np.zeros(N) # sound speed square
  alpha=np.zeros(N) # lapse
  log_alpha=np.zeros(N) # log of the conformal factorlapse

  rho[0]=rho0
  m[0] = 4./3.*np.pi*e[0]*r[0]**3
  p[0], e[0] = EOS_rho(rho[0], K, Gamma)
  
  SurfaceFound=False
  for i in range(N-1):
    if not SurfaceFound: # Interior
      # Call the EOS to get e
      e[i], rho[i], cs2[i]  = EOS_p (p[i], K, Gamma)
      # Solve TOV equations
      m[i+1] = m[i] + dr * 4*np.pi*e[i]*r[i]**2
      p[i+1] = p[i] - dr * (e[i]+p[i])*(m[i] + 4*np.pi*r[i]**3*p[i])/(r[i]*(r[i]-2*m[i]))
      isurf=i
      if p[i+1] <= 0. : # Surface found
        SurfaceFound=True
    else:   # Exterior
      m[i]=m[i-1]
      p[i]=0.
      rho[i]=0.
      e[i]=0.
      cs2[i]=0.

  if not SurfaceFound:
    print("ERROR: Surface not found")
      
  R = r[isurf] # Msun
  M = m[isurf] # Msun

  alpha = np.sqrt(1-2*m/r)
  grr = 1./(1-2*m/r)


  # Schwarzschild exterior 
  for i in np.arange(N-isurf)+isurf:
    alpha[i]=np.sqrt(1-2*m[i]/r[i])
    log_alpha[i] = np.log(alpha[i])
    
  # Interior lapse
  for i in np.arange(N-2, -1, -1):
    if p[i]>0:
      log_alpha[i] = log_alpha[i+1] - dr * (m[i] + 4*np.pi*r[i]**3*p[i])/(r[i]*(r[i]-2*m[i]))
      alpha[i] = np.exp(log_alpha[i])
      

  return R,  M, r[::stp], rho[::stp], p[::stp], e[::stp], cs2[::stp], alpha[::stp], grr[::stp], m[::stp]


def Schwazschild2Iso (rin, grrin, M, R):

  stp=100
  Nin=len(rin)
  N=Nin*stp

  rmax=rin[Nin-1]
  # Staggered grid (no points at 0 or rmax)
  dr=rmax/N
  r = np.arange(N)*dr + dr*0.5

  grr = CubicSpline(rin, grrin)

  phi=np.zeros(N) # conformal factor (metric in isotropic coordinates)
  log_phi=np.zeros(N) # log of the conformal factor

  isurf=np.where(r>R)[0][0]-1
  
  # Schwarzschild exterior in isotropic coordinates expressed as a function of the Schwarzschild radius
  for i in np.arange(N-isurf)+isurf:
    phi[i] = r[i]/M * (1. - np.sqrt(1.-2*M/r[i]))
    log_phi[i] = np.log(phi[i]) 
    
  # Integrate in the interior (from the surface to the center)
  for i in np.arange(N-2, -1, -1):
    if r[i]<isurf+1:
      log_phi[i] = log_phi[i+1] - dr * 0.5 * (1-np.sqrt(grr(r[i+1])))/r[i]
      phi[i] = np.exp(log_phi[i]) 

  
  riso=r/phi**2
  Riso=R/phi[isurf]**2
  
  return Riso, riso[::stp], phi[::stp]

