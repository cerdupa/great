import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

###########################################################################
def ReadBackground(dir,i):
    filename=("%s/background_%04d.dat" % (dir, i))
    if(os.path.isfile(filename)):
        print("------ Reading background file: %s ---------" % filename)
        data=np.loadtxt(filename)
        bg=pd.DataFrame({
          'time':data[:,0], 
          'nt':data[:,1], 
          'r':data[:,2], 
          'rho':data[:,3], 
          'eps':data[:,4], 
          'p':data[:,5], 
          'cs2':data[:,6], 
          'phi':data[:,7], 
          'alpha':data[:,8], 
          'h':data[:,9],
          'Gamma_1':data[:,10],
          'G':data[:,11],
          'N2':data[:,12],
          'L2':data[:,13],
          'B':data[:,14],
          'Q':data[:,15],
          'cs2_inv':data[:,16],
          'v_1':data[:,17],
          'omega':data[:,18],
          'y_e':data[:,19],
          'T':data[:,20],
          's':data[:,21],
          'U':data[:,22],
          'M':data[:,23],
        })
        return bg
    else:
        return 0
###########################################################################


###########################################################################
def ReadEigenfunctions(dir,i):
    filename=("%s/eigen_%04d.dat" % (dir, i))
    if(os.path.isfile(filename)):
        print("------ Reading eigenvalue file: %s ---------" % filename)
        data=np.loadtxt(filename)
        
        # Create a list with all different eigenvalues
        freqs=[]
        nf=[]
        for i in range(len(data[:,2])):
            if not (data[i,2] in freqs):
                freqs.append(data[i,2])
        EigenFreq=pd.DataFrame({"freqs":freqs})
        # Number of eigenvalues
        Nf=len(freqs)
        print("Number of eigenvalues  : ", Nf)


        # Number of radial cells
        Nr=int(len(data[:,1])/(Nf))
        print("Number of radial cells : ", Nr)
        
        
        EigenFunc=pd.DataFrame({})
        for freq in freqs:
            idx=np.where(data[:,2]==freq)
            r = data[idx][:,3]
            etar=data[idx][:,4]
            etap=data[idx][:,5]*r
            df=pd.DataFrame({
                'r':[r]
                ,'etar':[etar]
                ,'etap':[etap]
            })
#            EigenFunc=EigenFunc.append(df,ignore_index=True) ! deprecated
            EigenFunc = pd.concat([EigenFunc, df], ignore_index=True)
        return EigenFreq, EigenFunc
    else:
        return 0
###########################################################################
